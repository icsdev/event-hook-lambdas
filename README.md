# Create hook
## Description
Okta create hook to search for staged EUA users with a similar login, who needs to be activated.

It consists of two Lambda functions:

* Main
* Processor

Main one extracts IDs of created users from Okta Hook payload and asynchronously calls Processor Lambda to do actual work. Main Lambda will finish its job immediately after the invocation of Processor.

## Installation
1. Deploy CloudFormation template from `deployment.yml`, which will ask you to provide Okta URL/token as its parameters, as well as an API GW id  to allow it to call Main Lambda.
2. Run `docker run --rm -v $(pwd):/usr/src/app -w /usr/src/app node:alpine npm i && zip -r function.zip index.*js node_modules` in `main` folder and upload resulting `function.zip` as `createHook-MainFn` body.
3. Run `docker run --rm -v $(pwd):/usr/src/app -w /usr/src/app node:alpine npm i && zip -r function.zip index.*js node_modules` in `processor` folder and upload resulting `function.zip` as ` createHook-ProcessorFn` body.