const okta = require('@okta/okta-sdk-nodejs');

const client = new okta.Client();

exports.handler = async ({ userId: uid }) => {
    console.log(`${uid} - starting user processing`);

    try {
        const user = await client.getUser(uid);

        const login = user.profile.login;

        if (!login.includes('@')) {
            console.log(`${uid} - no @ in login`);
            return;
        }

        console.log(`${uid} - @ found in login`);

        if (login.indexOf('@') < 4) {
            console.log(`${uid} - login is less than 4 characters, skipping`);
            return;
        }

        const collidingLogin = login.substring(0, login.indexOf('@'));
        console.log(`${uid} - searching Okta for staged users with colliding login: ${collidingLogin}`);

        let userCnt = 0;
        const users = client.listUsers({ search: `profile.login eq "${collidingLogin}" and status eq "STAGED"` })

        for await (const user of users) {
            userCnt++;

            console.log(`${uid} - found matching user ${user.profile.login}, checking group membership`);

            const groups = await client.listUserGroups(user.id);

            for await (const group of groups) {
                if (group.profile.name === 'EUA Users' || group.profile.name === 'EUA-AD Users') {
                    console.log(`${uid} - user ${user.profile.login} is in EUA group ${group.profile.name}, activating`);

                    await client.activateUser(user.id, { sendEmail: false });
                    console.log(`${uid} - activated user ${user.profile.login}`);

                    break;
                }
            }

            console.log(`${uid} - group iteration finished`);
        }

        if (userCnt === 0) {
            console.log(`${uid} - no matching staged colliding users found`);
        }

        console.log(`${uid} - user processing finished`);
    } catch (err) {
        console.log(`${uid} - error: ${err}`);
    }
}