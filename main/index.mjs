import { LambdaClient, InvokeCommand } from "@aws-sdk/client-lambda";

const oktaVerificationHeaderName = 'X-Okta-Verification-Challenge';

const client = new LambdaClient({ region: process.env.REGION });

const headers = {
    'Content-Type': 'application/json',
};

export async function handler(event, _) {
    if (event.httpMethod === 'GET'
        && Object.keys(event.headers).includes(oktaVerificationHeaderName)) {
        return {
            body: JSON.stringify({ verification: event.headers[oktaVerificationHeaderName] }),
            statusCode: 200,
            headers: headers
        };
    }

    await parseEvent(event.body);

    return {
        body: '',
        statusCode: 204,
        headers: headers
    };
};

async function parseEvent(strBody) {
    const oktaEvent = JSON.parse(strBody);

    const promissesOuter = oktaEvent.data.events.map(async ev => {

        const promisses = ev.target.map(async tgt => {

            const params = {
                'FunctionName': process.env.PROCESSOR_FN_ARN,
                'InvocationType': 'Event',
                'Payload': JSON.stringify({ userId: tgt.id })
            };

            await client.send(new InvokeCommand(params));
            console.log('Invoked processor lambda for user: ' + tgt.id);
        });

        await Promise.all(promisses);
    });

    await Promise.all(promissesOuter);
}